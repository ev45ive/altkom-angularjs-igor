angular.module('tasks')

    .constant('INITIAL_TASKS', [])
    .constant('TASKS_API_URL', '')

    .service('Tasks', function Tasks(INITIAL_TASKS, TASKS_API_URL, $http, User) {
        console.log('Tasks Service created')


        this.state = {
            tasks: INITIAL_TASKS
        }

        this.getTasks = function () {
            return this.state.tasks
        }

        this.fetchTodos = function () {
            const user_id = User.getUser().id
            return $http.get(`http://localhost:3000/users/${user_id}/todos`)
                .then((res) => { return this.state.tasks = res.data })
        }

        this.fetchTaskById = function (id) {
            return $http.put(`${TASKS_API_URL}` + id).then(res => res.data)

        }

        this.updateTask = function (task) {
            return $http.put(`${TASKS_API_URL}` + task.id, task).then(res => res.data)

        }

        this.createTask = function (task) {
            task.userId = User.getUser().id;
            return $http.post(`${TASKS_API_URL}`, task).then(res => res.data)

        }

        this.deleteTask = function (id) {
            return $http.delete(`${TASKS_API_URL}` + id).then(res => res.data)

        }
    })

// tasks.factory('Tasks',function(){
//     return new Tasks()
// })