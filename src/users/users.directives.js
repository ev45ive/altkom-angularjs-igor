
const usersDirectives = angular.module('users')

usersDirectives.config(function (NavProvider) {
    NavProvider.addPage({
        label: 'Directives',
        url: 'src/users/user-directives.tpl.html'
    })
})

usersDirectives.directive('validEmail', function () {
    var pattern = /@/;
    return {
        require: 'ngModel',
        link: function (scope, el, attrs, ctrl) {
            // scope.$watch(attrs.ngModel, function () {
            //     var isValid = el.val().match(pattern) !== null;
            //     ctrl.$setValidity('time', isValid);
            // });
            ctrl.$validators.bademail = function(model,value){
                return String(value).match(pattern) !== null
            }
        }
    };
});

usersDirectives.directive('userCard', function () {
    return {
        restrict: 'E',
        scope: {
            title: '@',
            // localUser: '=userData',
            // localUser: '<userData',
            user: '<userData',
            save: '&onSave'
        },
        template: /* html */`<div class="card">
        <div class="card-body">
                <dl>
                    <dt>{{title}}:</dt>
                    <dd>{{localUser.name}}</dd>
                </dl>
                <input type="text" ng-model="localUser.name">
                <button ng-click="save({$event:localUser})">Save</button>
            </div>
        </div>`,
        controller(Tasks, $scope, User) {

            $scope.localUser = angular.copy($scope.user)

        },
        link(scope, $element, attrs, ctrl) {
            // scope.localUser = { name: 'placki' }

            // scope.save = function () {
            //     scope.$parent.$eval(attrs['onSave'], {
            //         $event: scope.localUser,
            //         extraDate: Date.now()
            //     })
            // }
            // // const userData = scope.$parent.$eval(attrs['userData'])

            // scope.$watch(attrs['userData'], (newVal, oldVal) => {
            //     scope.localUser = angular.copy(newVal)
            // })

        }
    }
})

// usersDirectives.directive('userCard', function () {
//     return {
//         restrict: 'E',
//         scope: true,
//         template: /* html */`<div class="card">
//             <div class="card-body">
//                 <dl>
//                     <dt>Name:</dt>
//                     <dd>{{localUser.name}}</dd>
//                 </dl>
//                 <input type="text" ng-model="localUser.name">
//                 <button ng-click="save()">Save</button>
//             </div>
//         </div>`,
//         link(scope, $element, attrs, ctrl) {
//             scope.localUser = { name: 'placki' }

//             scope.save = function () {
//                 scope.$parent.$eval(attrs['onSave'], {
//                     $event: scope.localUser,
//                     extraDate: Date.now()
//                 })
//             }
//             // const userData = scope.$parent.$eval(attrs['userData'])

//             scope.$watch(attrs['userData'], (newVal, oldVal) => {
//                 scope.localUser = angular.copy(newVal)
//             })

//         }
//     }
// })


usersDirectives.controller('DumbCtrl', () => { })
// appHiglight => app-higlight
// camelCase => camcel-case

usersDirectives.directive('appHiglight', function () {


    return {
        restrict: 'EACM',
        link(scope, $element, attrs, controller) {
            // console.log('hello', scope, $element, attrs, controller)

            $element.on('click', () => {
                $element.css('color', attrs.color || 'red')
                scope.name = 'Clicked!'
                scope.$digest()
            })
        }
    }
})


usersDirectives.directive('userDetails', function () {

    return {
        scope: { // isolated scope
            user: '=userModel', // evaluate attribute 'user-model' to scope.user
            edit: '&onEdit'
        },
        restrict: 'E', // only element <user-details></user-details>
        template: /* html */`<div>
            <dl>
                <dt>Name:</dt>
                <dd>{{user.name}}</dd>
                <dt>Email:</dt>
                <dd>{{user.email}}</dd>
                <dt>Bio:</dt>
                <dd>{{user.bio}}</dd>
            </dl>

            <button class="btn btn-info mt-4" ng-click="edit({
                $event: user
            })">
                Edit
            </button>
        </div>`
    }
})

usersDirectives.component('usersPage', {
    bindings: {},
    templateUrl: 'src/users/users-view.tpl.html',
    controller: 'UsersCtrl'
})

usersDirectives.component('usersList', {
    bindings: {
        users: '=users',
        selectedId: '<selectedId',
        select: '&onSelect',
        remove: '&onRemove'
    },
    // controllerAs: '$ctrl' // default name!
    template:/* html */`
    <div class="list-group">
        <div class="list-group-item" ng-repeat="user in $ctrl.users"
            ng-class="{active: $ctrl.selectedId === user.id}" 
            ng-click="$ctrl.select({$event: user.id })">
            {{user.name}}
            <span class="close float-end" ng-click="$ctrl.remove({$event:user.id})">&times;</span>
        </div>
    </div>`
})

usersDirectives.component('userForm', {
    bindings: {
        user: '<user',
        cancel: '&onCancel',
        save: '&onSave'
    },
    template:/* html */`
        <pre>{{ $ctrl.userForm | json}}</pre>

        <form name="$ctrl.userForm" ng-submit="$ctrl.submit()" novalidate> <!-- $scope.$ctrl.userForm -->
            <div class="form-group">
                <label for="">Name:</label>
                <input type="text" class="form-control" name="name" ng-model="$ctrl.draft.name"
                required minlength="3">

                <div ng-if="$ctrl.userForm.name.$touched || $ctrl.userForm.name.$dirty || $ctrl.userForm.$submitted">
                    <p class="text-danger" ng-if="$ctrl.userForm.name.$error.required">Field required</p>
                    <p class="text-danger" ng-if="$ctrl.userForm.name.$error.minlength">Value too short</p>
                </div>
            </div>

            <div class="form-group">
                <label for="">Email:</label>
                <input type="text" class="form-control" name="email"ng-model="$ctrl.draft.email" required  valid-email>  
                
                <div ng-if="$ctrl.userForm.email.$touched || $ctrl.userForm.email.$dirty || $ctrl.userForm.$submitted">
                    <p class="text-danger" ng-if="$ctrl.userForm.email.$error.required">Field required</p>
                    <p class="text-danger" ng-if="$ctrl.userForm.email.$error.bademail">Email invalid</p>
                </div>
            </div>

            <div class="form-group">
                <label for="">Bio:</label>
                <textarea class="form-control" name="bio" ng-model="$ctrl.draft.bio"></textarea>
            </div>

            <button class="btn btn-danger" ng-click="$ctrl.cancel()">Cancel</button>
            <button class="btn btn-danger" ng-click="$ctrl.reset()">Reset</button>
            <button class="btn btn-success" type="submit">Save Changes</button>
        </form>
        `,
    controller: function () {
        this.$onInit = function () { // props bound to this
            // this.draft = angular.copy(this.user)
        }

        this.$postLink = function () {
            debugger
        }

        this.$onChanges = function (changes) {
            this.draft = angular.copy(changes.user.currentValue)
        }

        this.$onDestroy = function () {
            // scope destroyed
        }

        // $scope.reset = function () { }
        this.reset = function () {
            this.draft = {}
        }
        this.submit = function () {
            if (this.userForm.$invalid) {
                // this.userForm.$setSubmitted(true)
                return;
            }

            this.save({ $event: this.draft })
        }
    }
})
// usersDirectives.directive('userForm', function () {

//     return {
//         restrict: 'E',
//         scope: { // isolated scope
//             // draft: '=user ', // evaluate attribute 'user-model' to scope.user
//             user: '<user ', // evaluate attribute 'user-model' to scope.user
//             cancel: '&onCancel',
//             save: '&onSave'
//         },
//         bindToController: true,
//         template:/* html */`
//         <div>
//             <div class="form-group">
//                 <label for="">Name:</label>
//                 <input type="text" class="form-control" ng-model="$ctrl.draft.name">
//             </div>

//             <div class="form-group">
//                 <label for="">Email:</label>
//                 <input type="text" class="form-control" ng-model="$ctrl.draft.email">
//             </div>

//             <div class="form-group">
//                 <label for="">Bio:</label>
//                 <textarea class="form-control" ng-model="$ctrl.draft.bio"></textarea>
//             </div>

//             <button class="btn btn-danger" ng-click="$ctrl.cancel()">Cancel</button>
//             <button class="btn btn-danger" ng-click="$ctrl.reset()">Reset</button>
//             <button class="btn btn-success" ng-click="$ctrl.submit()">Save Changes</button>
//         </div>
//         `,
//         controller: function ($scope) {
//             // this.draft = this.user // Not props not bound yet

//             this.$onInit = function () { // props bound to this
//                 // this.draft = angular.copy(this.user)
//             }

//             this.$onChanges = function (changes) {
//                 this.draft = angular.copy(changes.user.currentValue)
//             }


//             this.$onDestroy = function () {
//                 // scope destroyed
//             }

//             // $scope.reset = function () { }
//             this.reset = function () {
//                 this.draft = {}
//             }
//             this.submit = function () {
//                 this.save({ $event: this.draft })
//             }
//         },
//         controllerAs: '$ctrl'
//     }

// })