// import {} from './app.module' 
// export {}

const config = angular.module('config', [])

config.constant('TASKS_API_URL', 'http://localhost:3000/todos/')

config.constant('INITIAL_TASKS', [
    { id: 1, title: 'Learn Angular', completed: false },
    { id: 2, title: 'Learn ngRepeat', completed: true },
])

config.constant('USERS_INITIAL_DATA', [
    {
        "id": 1,
        "name": "Leanne Graham",
        "username": "Bret",
        "email": "Sincere@april.biz",
    },
    {
        "id": 2,
        "name": "Ervin Howell",
        "username": "Antonette",
        "email": "Shanna@melissa.tv",
    }
])

config.value('SESSION_ID', '');

/* Config phase */
angular.module('config').config(function (UsersProvider, TASKS_API_URL) {
    UsersProvider.setInitialData(TASKS_API_URL)
    UsersProvider.setApiUrl('http://localhost:3000/users')
})

/* Run phase */

angular.module('config').run(function (Users) {
    Users.fetchUsers()
})

// angular.module('config').run(function ($http) {
//     $http.defaults.headers.common.Authorization = 'Basic YmVlcDpib29w';
// });

/* HTTP */

angular.module('config').config(function ($httpProvider) {

    $httpProvider.interceptors.push(function ($rootScope, $q/* , BackupApiService, TokenService */) {
        return {
            'request': function (config) {

                config.headers.Authorization = 'Basic YmVlcDpib29w';
                // config.headers.Authorization = TokenService.getCurrentToken();
                return config;
            },

            'responseError': function (response) {
                $rootScope.notify('Server error ' + response.statusText)

                // Backup request
                // if(response.url == '...1'){
                //     return BackupApiService.get('...2')
                // }
                // return $q.resolve({ data: [] })

                return $q.reject(response)
            }
        };
    })

});
