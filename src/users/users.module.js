

const users = angular.module('users', [])

users.constant('USERS_INITIAL_DATA', [])

angular.module('users').config(function (NavProvider) {
    NavProvider.addPage({
        label: 'Users',
        url: 'src/users/users-view.tpl.html'
    })
})


users.controller('UsersCtrl', function ($scope, USERS_INITIAL_DATA) {

    this.users = angular.copy(USERS_INITIAL_DATA)
    this.mode = 'details'
    this.selectedId = 1

    this.draft = this.users[0];

    this.select = function (id) {
        // this.mode = 'details'
        this.selectedId = id
        this.draft = (this.users.find(u => u.id == this.selectedId))
    }

    this.createNew = function () {
        this.selectedId = 0
        this.draft = {}
        this.mode = 'edit'
    }

    this.edit = function (id) {
        this.mode = 'edit'
        this.selectedId = id
        this.draft = angular.copy(this.users.find(u => u.id == this.selectedId))
    }

    this.remove = function (id) {
        const index = this.users.findIndex(u => u.id === id)
        if (index !== -1) {
            this.users.splice(index, 1)
        }
    }
    this.cancel = function () {
        this.selectedId = null
        this.draft = null
        this.mode = 'details'
    }
    this.save = function (draft) {
        if (draft.id) {
            const index = this.users.findIndex(u => u.id === this.selectedId)
            if (index !== -1) {
                this.users.splice(index, 1, draft)
            }
        } else {
            draft.id = Date.now()
            this.users.push(draft)
        }
        this.selectedId = (draft.id)
    }
})