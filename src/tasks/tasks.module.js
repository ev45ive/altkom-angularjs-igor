angular.module('tasks', [])

angular.module('tasks').config(function(NavProvider){
    NavProvider.addPage({
        label: 'Tasks',
        url: 'src/tasks/tasks-view.tpl.html'
    })
})


angular.module('tasks').controller('TasksCtrl', function ($scope, Tasks) {
    console.log('Tasks controller created')
    $scope.$on('$destroy', () => console.log('Tasks controller destroyed'))

    $scope.state = Tasks.state;

    // Pull state
    $scope.updateList = function () {
        $scope.loading = true
        Tasks.fetchTodos().then(data => {
            console.log(data)
            $scope.loading = false
        })
    }
    $scope.updateList()


    $scope.recalculate = () => { $scope.completedCount = $scope.state.tasks.filter(t => t.completed).length }
    $scope.$watchCollection('state.tasks', (newVal, oldVal) => { $scope.recalculate() })

    $scope.completedCount = 0

    $scope.selectedId = null

    $scope.tempTitle = ''

    $scope.addTask = function () {
        Tasks.createTask({
            title: $scope.tempTitle,
            completed: false
        }).then(() => {
            $scope.updateList()
            $scope.tempTitle = ''
        })
    }

    $scope.removeTask = (id) => {
        Tasks.deleteTask(id).then(() => {
            $scope.updateList()
        })
    }

    $scope.selectTask = (task_id) => {
        $scope.selectedId = task_id
    }

    $scope.taskCompleted = ($event, task) => {
        $event.stopPropagation()
        task.completed = !task.completed;

        Tasks.updateTask(task).then(() => {
            $scope.updateList()
        })
    }
})
