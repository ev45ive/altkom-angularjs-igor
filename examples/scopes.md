```js
function Scope(){
    this.$id = 1
}
Scope.prototype.id = 1
Scope.prototype.$new = function(){
    var childScope = Object.create(this)
    childScope.$id = ++Scope.prototype.id
    return childScope 
}

rootScope = new Scope()

rootScope.user = {name:''}

s2 = rootScope.$new()
s3 = s2.$new()

s2.user.name = 'Admin'
s3.user


{name: "Admin"}

```