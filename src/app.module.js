
angular.module('nav', []).provider('Nav', function () {

    let pages = []
    return {
        addPage(page) {
            pages.push(page)
        },
        $get: function () {
            return new function NavService() {
                this.pages = pages
            }
        }
    }
})


// angular.module('users').config(function (NavProvider) {
//     NavProvider.addPage({
//         label: 'Users',
//         url: 'src/users/users-view.tpl.html'
//     })
// })


// var app = angular.module('app') // Get existing module
var app = angular.module('myapp', [
    'nav',
    'users',
    'tasks',
    'config'
]) // Create new module with [] deps


app.service('User', function () {
    this.state = {
        user: { id: 1, name: 'Admin' }
    }

    this.login = function () { }
    this.logout = function () { }
    this.getUser = function () {
        return this.state.user
    }
})


app.controller('AppCtrl', ($scope, $rootScope, $timeout, User, Nav) => {

    $rootScope.notifications = []
    $rootScope.notify = function (message) {
        $rootScope.notifications.push(message)
        $timeout(function () {
            const index = $rootScope.notifications.indexOf(message)
            $rootScope.notifications.splice(index, 1)
        }, 4000)
    }

    // $scope.user = User.state;
    $scope.$watch('Task.state.user', () => {
        $scope.user = User.getUser();
    })

    $scope.time = ''

    // setInterval(() => {
    //     $scope.time = (new Date()).toLocaleTimeString()
    //     $scope.$digest()
    // }, 1000)

    // $interval(() => {
    //     $scope.time = (new Date()).toLocaleTimeString()
    // }, 1000)

    $scope.login = function () {
        $scope.user.name = 'Admin'
    }
    $scope.logout = function () {
        $scope.user.name = ''
    }

    $scope.pages = Nav.pages;
    $scope.currentPage = $scope.pages[0]

    $scope.goToPage = (page) => {
        $scope.currentPage = page;
    }
    // debugger
})

angular.bootstrap(document, ['myapp'])