angular.module('users')

    // .service('Users', function (USERS_INITIAL_DATA) {
    //     this.fetchUsers = function () { }
    // })

    // .factory('Users', function (USERS_INITIAL_DATA) {
    //     function UsersService() { }

    //     const service = new UsersService()
    //     service.setUrl('...')

    //     return service;
    // })

    .provider('Users', function () {

        let initialData, apiUrl;

        return {
            // Config Phase
            setInitialData(data) { initialData = data },
            setApiUrl(url) { apiUrl = url },
            // setSessionService(service){}

            // Factory / Run Phase
            $get($http) {
                return new UsersService(initialData, apiUrl, $http)
            }
        }
    })

function UsersService(initialData, apiUrl, $http) {
    this.fetchUsers = function () {
        // const user_id = sessionService.getUserID()
        $http.get(apiUrl).then(console.log)
    }

}

